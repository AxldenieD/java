package jeu;

/**
 * @author maillot
 */
public class AssemblageDeDominos {

    private String milieu;
    private Domino gauche;
    private Domino droite;

    public AssemblageDeDominos() {
        milieu = "";
        gauche = null;
        droite = null;
    }
    @Override
    public String toString() {
        return (gauche != null ? gauche : "") + milieu + (droite != null ? droite : "");
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AssemblageDeDominos other = (AssemblageDeDominos) obj;
        if ((this.milieu == null) ? (other.milieu != null) : !this.milieu.equals(other.milieu)) {
            return false;
        }
        if (this.gauche != other.gauche && (this.gauche == null || !this.gauche.equals(other.gauche))) {
            return false;
        }
        if (this.droite != other.droite && (this.droite == null || !this.droite.equals(other.droite))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 73 * hash + (this.milieu != null ? this.milieu.hashCode() : 0);
        hash = 73 * hash + (this.gauche != null ? this.gauche.hashCode() : 0);
        hash = 73 * hash + (this.droite != null ? this.droite.hashCode() : 0);
        return hash;
    }

    public boolean ajouterGauche(Domino d) {
        if (this.gauche == null && this.droite == null) {
            this.gauche = d;
            return true;
        } else if (this.droite == null) {
            if (d.getDroite() == gauche.getGauche()) {
                droite = gauche;
                gauche = d;
                return true;
            } else {
                return false;
            }
        } else {
            if (d.getDroite() == gauche.getGauche()) {
                milieu = gauche + milieu;
                gauche = d;
                return true;
            } else {
                return false;
            }
        }
    }

    public boolean ajouterDroite(Domino d) {
        if (this.gauche == null && this.droite == null) {
            this.gauche = d;
            return true;
        } else if (this.droite == null) {
            if (d.getGauche() == gauche.getDroite()) {
                droite = d;
                return true;
            } else {
                return false;
            }
        } else {
            if (d.getGauche() == droite.getDroite()) {
                milieu = milieu + droite;
                droite = d;
                return true;
            } else {
                return false;
            }
        }
    }

    public boolean ajouter(Domino d) {
        if (ajouterGauche(d)) {
            return true;
        }
        if (ajouterDroite(d)) {
            return true;
        }
        d.inverser();
        if (ajouterGauche(d)) {
            return true;
        }
        if (ajouterDroite(d)) {
            return true;
        }
        return false;
    }
}
