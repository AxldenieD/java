package jeu;

import interfaces.Inversible;

public class Domino implements Inversible{
    
    private int gauche;
    private int droite;

    // TODO: 3.2 Constructeur
    // public Domino(int a, int b)
    public Domino(int a, int b) throws Exception
    {
    	if((a>0 && a<7) && (b>0 && b<7))
    	{
    		this.gauche=a;
    		this.droite=b;
    	}
    	else
    	{
    		throw new Exception("param�tre incorrect");
    	} 
    	
    }
    public int getDroite() {
        return droite;
    }

    public int getGauche() {
        return gauche;
    }

    // TODO: 3.4 Redéfinition des méthodes d’Object
    // public boolean equals(Object obj)
    // public int hashCode()
    // public Domino clone()

    @Override
    public String toString() {
        return "[" + gauche + "|" + droite + "]";
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + droite;
		result = prime * result + gauche;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Domino other = (Domino) obj;
		if((droite==other.droite && gauche==other.gauche) || (droite==other.gauche && gauche==other.droite))
		{
			return true;
		}
		else return false;
	}

    // TODO: 3.5 Implémentation
	public Domino clone() throws CloneNotSupportedException
	{
			return (Domino) super.clone();
	}
	//------- Impl�mentation d'inverser
	public void inverser()
	{
		int temp=this.gauche;
		this.gauche=this.droite;
		this.droite=temp;
		
	}
}
