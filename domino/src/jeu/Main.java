package jeu;

import java.util.Random;
import java.util.Scanner;

/**
 * 4.5 Tests.
 * Écrire une application autonome qui ajoute des dominos tirés au hasard dans
 * un assemblage de dominos vide au départ. Les ajouts se font et sont affichés
 * tant qu'ils réussissent.
 * @author Yvan
 */
public class Main {
    
    // TODO: Récupérer l'exception et la traiter (afficher des infos sur cette
    // exception dans la console)
    public static void main(String[] args) {
        Random r = new Random();
        AssemblageDeDominos ad;
        Scanner s = new Scanner(System.in);
        int n;
        do {
            n = 0;
            ad = new AssemblageDeDominos();
            System.out.println(ad);
            try {
				while (ad.ajouter(new Domino(r.nextInt(6)+1, r.nextInt(6)+1))) {
				    System.out.println(ad);
				    n++;
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.out.println(e.toString());
			}
        } while (n < 10);
    }
}
