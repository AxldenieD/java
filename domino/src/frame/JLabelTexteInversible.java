package frame;

import interfaces.Inversible;

import javax.swing.Icon;
import javax.swing.JLabel;

/**
 * @author maillot
 */
public class JLabelTexteInversible extends JLabel implements Inversible{

	public JLabelTexteInversible(Icon image) {
        super(image);
    }

    public JLabelTexteInversible(Icon image, int horizontalAlignment) {
        super(image, horizontalAlignment);
    }

    public JLabelTexteInversible(String text) {
        super(text);
    }

    public JLabelTexteInversible(String text, int horizontalAlignment) {
        super(text, horizontalAlignment);
    }

    public JLabelTexteInversible(String text, Icon icon, int horizontalAlignment) {
        super(text, icon, horizontalAlignment);
    }
    public void inverser()
    {
    	String t=this.getText();
    	int nb=t.length();
    	char[] tab=t.toCharArray();
    	char[] temp= new char[nb];
    	for(int i=0;i<tab.length;i++)
    	{
    		temp[nb-i-1]=tab[i];
    	}
    //	String res=temp.toString();
    	String p=new String(temp);
    	System.out.println(p);
    	this.setText(p);
    	
    }
    // TODO: 5.2 JLabelTexteInversible
}
