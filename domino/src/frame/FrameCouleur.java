package frame;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 * @author maillot
 */
public class FrameCouleur extends JFrame {
    private JLabelTexteInversible label = new JLabelTexteInversible("Texte", JLabel.CENTER);
    private JButton bouton = new JButton("<>");
    public FrameCouleur() {
        super("Test");
        getContentPane().add(label, "Center");
        getContentPane().add(bouton, "East");

        bouton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                label.inverser();
            }
        });
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);
        pack();
    }
}
