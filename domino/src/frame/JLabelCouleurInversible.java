package frame;

import java.awt.Color;

import interfaces.Inversible;

import javax.swing.Icon;
import javax.swing.JLabel;

/**
 * @author maillot
 */
public class JLabelCouleurInversible extends JLabel implements Inversible{
    
	private Color Background;
	private Color Foreground;
	
	private void updateI()
	{
    	this.setBackground(this.Background);
    	this.setForeground(this.Foreground);
    	this.updateUI();
	}
	
	public JLabelCouleurInversible(Color back, Color fore) {
    	this.setOpaque(true);
    	this.Background=back;
    	this.Foreground=fore;

    	this.updateI();
    }

    public Color getBackground() {
		return Background;
	}


	public void setBackground(Color background) {
		Background = background;
	}


	public Color getForeground() {
		return Foreground;
	}


	public void setForeground(Color foreground) {
		Foreground = foreground;
	}


	public JLabelCouleurInversible(Icon image) {
        super(image);
    }

    public JLabelCouleurInversible(Icon image, int horizontalAlignment) {
        super(image, horizontalAlignment);
    }

    public JLabelCouleurInversible(String text) {
        super(text);
    }

    public JLabelCouleurInversible(String text, int horizontalAlignment) {
        super(text, horizontalAlignment);
    }

    public JLabelCouleurInversible(String text, Icon icon, int horizontalAlignment) {
        super(text, icon, horizontalAlignment);
    }
    
		public void inverser()
		{
			Color temp=this.Background;
			this.Background=this.Foreground;
			this.Foreground=temp;
			this.updateI();
			System.out.println("back : "+this.getBackground().toString());
	    	System.out.println("fore : "+this.getForeground().toString());
		}
    // TODO: 5.1 JLabelCouleurInversible
}
