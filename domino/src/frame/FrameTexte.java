package frame;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 * @author maillot
 */
public class FrameTexte extends JFrame {
    private JLabelCouleurInversible label; /*= new JLabelCouleurInversible("couleur", JLabel.CENTER);*/
    private JButton bouton = new JButton("<>");
    public FrameTexte(Color a, Color b) {
        super("Test");
        this.label= new JLabelCouleurInversible(a, b);
        getContentPane().add(label, "Center");
        getContentPane().add(bouton, "East");

        bouton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                label.inverser();
             //   System.out.println(label.getText());
            }
        });

        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);
        pack();
    }
}
