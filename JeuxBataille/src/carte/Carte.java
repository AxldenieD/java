package carte;

public class Carte {

private String couleur;
private String figure;
private int valeur;
private int nbPoints;

	public Carte(String coul, String fig, int v, int nbP)
	{
		this.couleur=coul;
		this.figure=fig;
		this.valeur=v;
		this.nbPoints=nbP;
	}
	public Carte(String coul, String fig, int v)
	{
		this(coul, fig, v,0);
	}
	public Carte(String coul, String fig)
	{
		this(coul, fig, 0,0);
	}
	public Carte(String coul)
	{
		this(coul, "", 0,0);
	}
	
	public String GetCoul()
	{
		return this.couleur;
	}
	
	public String GetFig()
	{
		return this.figure;
	}
	public int GetVal()
	{
		return this.valeur;
	}
	public int GetNb()
	{
		return this.nbPoints;
	}
	
}
