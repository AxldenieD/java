package principal;

public class Sudoku {
	private Case[][] grille;
	
	
	public Sudoku() throws Exception
	{
		this.grille = new Case[9][9];
		for(int i=0;i<9;i++)
		{
			for(int j=0;j<9;j++)
			{
			grille[i][j]=new Case('3');	
			}
		}
	}
	
	private boolean TestCase(int l, int c)
	{
		return (l>=0 && l<=9 && c>=0 && c<=9);
	}
	
	public char GetCase(int l, int c)
	{
		if(TestCase(l,c))return grille[l][c].getChiffre();	

		else 
			{
			System.out.println("case incorrecte !");
			return '0';
			}
	}
	
	public void SetCase(int l,int c, char k) throws Exception
	{
		if(TestCase(l,c))
		{//la case est ok 
		//	on doit tester si la case est fig�e
			if(!grille[l][c].isFigee())grille[l][c].setChiffre(k);
			else System.out.println("case fig�e modification impossible");
		} else System.out.println("case incorrecte !"); 
	}
	
	public void affiche()
	{
		for(int i=0;i<9;i++)
		{
			for(int j=0;j<9;j++)
			{
				System.out.print(grille[j][i].toString());
				if((j+1)%3==0)System.out.print(' ');
				
			}
			if((i+1)%3==0)System.out.println("");
			System.out.println("");
		}
		
	}
	
	
	public boolean ligneValide(int l)
	{
	
		for(int i=0;i<8;i++)
		{
			for(int j=i+1;j<9;j++)
			{
				if(Case.equals(grille[l][i],grille[l][j]) || !grille[l][j].estRemplie())return false;
			}
		}
		
		
		return true;
	}
}
