package principal;

public class Case {
	
private final boolean figee;
private char chiffre;
public static boolean equals(Case a, Case b)
{
return(a.getChiffre() == b.getChiffre());
}
/*Constructeur*/
	public Case()
	{
		this.figee=false;
		this.chiffre=' ';
	}
/*Surcharge*/
	public Case(char c) throws Exception
	{
		if(!TestChar(c))
		{
			throw new Exception("chiffre incorrect");
		}
		else
		{
			this.figee=true;
			this.chiffre=c;	
		}
	}
	
	private boolean TestChar(char c)
	{
		
		return(c>='1' && c<='9');
	}
	
	public Case clone()
	{
		Case n=null;
		if(this.isFigee())
		{
			try {
				n = new Case(this.chiffre);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else 
		{
		n=new Case();
		try {
			n.setChiffre(this.chiffre);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
		
		return n;
	}
	public char getChiffre()
	{
		return chiffre;
	}
	public boolean isFigee()
	{
		return figee;
	}
	
	public void setChiffre(char c) throws Exception
	{
		if(!TestChar(c) || this.isFigee())
		{
			throw new Exception("Chiffre incorrect ou op�ration non autoris�e");
		}
		else this.chiffre = c;
	}
	public void efface() throws Exception
	{
		if(this.isFigee())
		{
			throw new Exception("Case fig�e il est interdit de la vider");
		}
		else this.chiffre=' ';
	}
	public boolean estRemplie()
	{
		if(this.chiffre==' ')return false;
		else return true;
		
	}
	public String toString()
	{
		if(this.isFigee())return "["+this.chiffre+"]"; 
		else return "("+this.chiffre+")";
	}
	

	
}
