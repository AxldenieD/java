package parking;

public class Vehicule implements Cloneable {
private int nbRoues;
private int nBPortes;

		public Vehicule(int r, int p)
		{
			this.nbRoues = r;
			this.nBPortes = p;
		}
		public Vehicule(int p)
		{
			this(4,p);
		}
		public Vehicule()
		{
			this(4,4);
		}
		
		public String toString()
		{
			return "Nombre de roue : "+ this.nbRoues + " Nombre Portes : "+ this.nBPortes;
		}
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Vehicule other = (Vehicule) obj;
			if (nBPortes != other.nBPortes)
				return false;
			if (nbRoues != other.nbRoues)
				return false;
			return true;
		}
		
		public Vehicule clone() throws CloneNotSupportedException
		{
			return (Vehicule) super.clone();
		}
	public int getNbRoues()
	{
	return nbRoues;
	}
	public int getnBPortes()
	{
	return nBPortes;
	}
		
}
