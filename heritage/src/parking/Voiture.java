package parking;

public class Voiture extends Vehicule implements Cloneable{
	
	private boolean toitOuvrant;
	private Moteur mot;
	
	/* Constructeurs*/
	public Voiture(boolean toit, int portes)
	{
		super(portes);
		this.toitOuvrant=toit;
		this.mot=new Moteur();
	}
		public Voiture(int portes)
	{
		this(false,portes);
	}
	public Voiture()
	{
		this(false,4);
	}
	/**/
		
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Voiture other = (Voiture) obj;
		if (mot == null) {
			if (other.mot != null)
				return false;
		} else if (!mot.equals(other.mot))
			return false;
		if (toitOuvrant != other.toitOuvrant)
			return false;
		return true;
	}
	public void tune()
	{
		this.toitOuvrant=!this.toitOuvrant;
	}
	public String toString()
	{
		String val;
		if(this.toitOuvrant==true) val="oui";
		else val="non";
		return super.toString()+" toit ouvrant : " + val + " " + this.mot.toString();
	}
	public void gonfle()
	{
		this.mot.gonfler();
	}
	public void bride()
	{
		this.mot.brider();
	}
	
	public void setToitOuvrant(boolean toitOuvrant) {
		this.toitOuvrant = toitOuvrant;
	}
	public Voiture clone() throws CloneNotSupportedException
	{
		Voiture temp;
			temp=(Voiture) super.clone();/*PAS de NEW DANS METHODE CLONE !! on utilise SUPER.clone() et m�me si la classe n'�h�rite de rien, on utilise quand m�me super.clone qui appelera le clone de la classe Object*/
		//	temp.setToitOuvrant(this.toitOuvrant);
			temp.toitOuvrant=this.toitOuvrant;
			temp.mot=(Moteur)this.mot.clone();
		return temp;
	}
}
