package parking;

public class Moteur implements Cloneable{
private double cylindre;
	public Moteur()
	{
		this.cylindre=2;
	}
	public String toString()
	{
		return "cylindr�e : " + this.cylindre;
	}
	
	@Override
	public Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		Moteur temp;
		temp=(Moteur) super.clone();
		temp.cylindre=this.cylindre;
		return temp;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Moteur other = (Moteur) obj;
		if (Double.doubleToLongBits(cylindre) != Double
				.doubleToLongBits(other.cylindre))
			return false;
		return true;
	}
	
	public void brider()
	{
		this.cylindre=this.cylindre / 1.5;
	}
	public void gonfler()
	{
		this.cylindre=this.cylindre*1.5;
	}

}
