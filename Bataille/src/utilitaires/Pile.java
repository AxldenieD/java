package utilitaires;

import carte.Carte;

/**
 * @author Y. Maillot
 */
public class Pile {

    private Carte[] pile;
    private int sommet;

    public Pile(int n) {
        pile = new Carte[n];
        sommet = 0;
    }

    public void empiler(Carte c) {
        pile[sommet] = c;
        this.sommet++;
 
    }

    public Carte depiler() {
        sommet--;
        return pile[sommet];
    }

    public Carte sommet() {
        return pile[sommet - 1];
    }

    public boolean vide() {
        return sommet == 0;
    }
}
