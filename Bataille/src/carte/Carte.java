/*
Les cartes
	- figures (7, roi, valet, 21, 1, excuse)
	- couleurs (pique, coeur, carreau, trefle, atout, ..., famille cochon, famille duChnok)
	- Une valeur relative (qui dépend parfois de la situation de jeu)
	- Des points pour le comptage
*/

package carte;

/**
 * @author Y. Maillot & J. Lepagnot
 */
public class Carte {
    private String figure;
    private String couleur;
    private int valeur;
    private int nbPoints;

    /**
     * Pour créer une carte avec tous les paramètres.
     * @param figure un mot décrivant la figure ("roi", "as", "7", ...)
     * @param couleur un mot décrivant la couleur ("coeur", "pique", ...)
     * @param valeur un entier donnant une valeur relative de cette carte par rapport aux autres
     * @param nbPoints le nombre de points de cette carte
     */
    public Carte(String figure, String couleur, int valeur, int nbPoints) {
        this.figure = figure;
        this.couleur = couleur;
        this.valeur = valeur;
        this.nbPoints = nbPoints;
    }

    /**
     * Pour créer une carte avec les paramètres suivants et dont le nombre de points vaut 0.
     * @param figure un mot décrivant la figure ("roi", "as", "7", ...)
     * @param couleur un mot décrivant la couleur ("coeur", "pique", ...)
     * @param valeur un entier donnant une valeur relative de cette carte par rapport aux autres
     */
    public Carte(String figure, String couleur, int valeur) {
        this(figure, couleur, valeur, 0);
    }

    /**
     * Pour créer une carte avec les paramètres suivants, dont la valeur relative et
     * le nombre de points valent 0.
     * @param figure un mot décrivant la figure ("roi", "as", "7", ...)
     * @param couleur un mot décrivant la couleur ("coeur", "pique", ...)
     */
    public Carte(String figure, String couleur) {
        this(figure, couleur, 0, 0);
    }

    /**
     * Pour créer une carte avec les paramètres suivants, dont la couleur est le mot vide,
     * la valeur relative et le nombre de points valent 0.
     * @param figure un mot décrivant la figure ("roi", "as", "7", ...)
     */
    public Carte(String figure) {
        this(figure, "", 0, 0);
    }

    /**
     * Accesseur.
     */
    public String getCouleur() {
        return couleur;
    }

    /**
     * Accesseur.
     */
    public String getFigure() {
        return figure;
    }

    /**
     * Accesseur.
     */
    public int getNbPoints() {
        return nbPoints;
    }

    /**
     * Accesseur.
     */
    public int getValeur() {
        return valeur;
    }


    /**
     * Affichage d'une description de la carte.
     */
    public void affiche() {
        System.out.print(figure + " de " + couleur + " ");
    }
}
