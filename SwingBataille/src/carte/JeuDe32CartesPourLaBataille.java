package carte;

import java.util.Random;

/**
 * @author Y. Maillot
 */
public class JeuDe32CartesPourLaBataille {
    private Carte[] jeu;
    private static String[] couleur = {"pique", "coeur", "carreau", "trefle"};
    private static String[] figure = {"7", "8", "9", "10", "Valet", "Dame", "Roi", "As"};

    public JeuDe32CartesPourLaBataille() {
        jeu = new Carte[couleur.length * figure.length];
        int k = 0;
        for (int i = 0; i < couleur.length; i++) {
            for (int j = 0; j < figure.length; j++) {
                jeu[k] = new Carte(figure[j], couleur[i], j);
                k++;
            }   
        }
    }
    
    public void affiche() {
        for(Carte c : jeu) {
            c.affiche();
            System.out.println();
        }  
    }

    public void battre() {
        Random random = new Random();
        int nbfois = jeu.length + random.nextInt(jeu.length+1);
        for(int i = 0; i < nbfois; i++) {
            permuter(random.nextInt(jeu.length), random.nextInt(jeu.length));
        }
    }

    private void permuter(int i1, int i2) {
        Carte c = jeu[i1];
        jeu[i1] = jeu[i2];
        jeu[i2] = c;
    }

    public void couper() {
        Random random = new Random();
        int nbCartes = random.nextInt(jeu.length);
        // donne le nombre de cartes déplacées
        // à la fin du jeu
        if (nbCartes > 0) {
            // Création d'un paquet temporaire de cartes
            Carte[] tas = new Carte[nbCartes];
            for(int i = 0; i < nbCartes; i++) {
                tas[i] = jeu[i];
            }
            // Décaler le paquet
            for(int i = nbCartes; i < jeu.length; i++) {
                jeu[i-nbCartes] = jeu[i];
            }
            // Recopie du paquet temporaire
            for(int i = 0; i < tas.length; i++) {
                jeu[jeu.length-tas.length+i] = tas[i];
            }
        }
    }

    public Carte get(int i) {
        return jeu[i];
    }

    public int nbCartes() {
        return jeu.length;
    }
}
