package jeu;
import carte.JeuDe32CartesPourLaBataille;
import java.util.Random;
import utilitaires.Pile;

/**
 * @author Y. Maillot & J. Lepagnot
 */
public class Bataille {
    private JeuDe32CartesPourLaBataille jeu;
    private Joueur j1, j2;
    private Pile tas1, tas2;
    private static Random random = new Random();
    private Joueur gagnant = null;
    private VisionnageBataille vB;
    public Joueur getGagnant() {
        return gagnant;
    }
    public Bataille(Joueur j1, Joueur j2) {
        this.j1 = j1;
        this.j2 = j2;
        jeu = new JeuDe32CartesPourLaBataille();
        tas1 = new Pile(16);
        tas2 = new Pile(16);
        vB=new VisionnageBataille(j1.getNom(), j2.getNom());
        jouerUnePartie();
    }
    public Bataille(String n1, String n2) {
        this(new Joueur(n1), new Joueur(n2));
    }
    private void jouerUnePartie() {
        jeu.battre();
        jeu.couper();
        distribuer();
        // Faire des plis tant que les deux joueurs ont (au moins) une carte chacun.
        while (j1.aUneCarte() && j2.aUneCarte()) {
            faireUnPli();
        }
        // Sortie de cette boucle si
        //   (1) seul j2 n'a plus de carte
        //       Dans ce cas j1 gagne.
        //   (2) seul j1 n'a plus de carte
        //       Dans ce cas j2 gagne
        //   (3) Aucun n'a de carte, c'est le cas rare du match nul
        if (j1.aUneCarte()) { // (1)
            ramasse(j1); // Il peut rester des cartes sur la table lorsque
                         // j2 n'a pas pu aller "au bout" d'une bataille.
            gagnant(j1);

        } else if (j2.aUneCarte()) { // (2)
            ramasse(j2); // Il peut rester des cartes sur la table lorsque
                         // j1 n'a pas pu aller "au bout" d'une bataille.
            gagnant(j2);
        } else { // (3)
            gagnant();
        }
    }
    private void distribuer() {
        for (int i = 0; i < jeu.nbCartes(); i += 2) {
            j1.ranger(jeu.get(i));
            j2.ranger(jeu.get(i + 1));
        }
    }
    private void affichageMainDesJoueurs() {
        System.out.print(j1.getNom() + " a ");
        j1.affiche();
        
        System.out.print(" <--> ");
        System.out.print(j2.getNom() + " a ");
        j2.affiche();
        System.out.println("");
    }

    private void affichageActions() {
        System.out.print(j1.getNom() + " pose un ");
        tas1.sommet().affiche();
        System.out.print(" <--> ");
        System.out.print(j2.getNom() + " pose un ");
        tas2.sommet().affiche();
        System.out.println("");
    }

    private void faireUnPli() {
        // *** Affichage
        affichageMainDesJoueurs();
        // *** Affichage

        // Chaque joueur lance une carte, pour ça il faut encore qu'ils en
        // aient chacun au moins une... Mais c'est bien le cas car cette méthode
        // est lancée depuis la boucle while (j1.aUneCarte() && j2.aUneCarte())
        // qui nous garantit ça.
        this.tas1.empiler(j1.lancer());
        this.tas2.empiler(j2.lancer());

        // *** Affichage
        affichageActions();
        affichageMainDesJoueurs();
        // *** Affichage

        // Les cartes aux sommets des deux tas sont visibles
        boolean faceVisible = true;

        // Il faut gérer les "batailles"
        // En cas d'égalité des cartes aux sommets des deux tas
        // ET SI LES DEUX ONT ENCORE UNE CARTE (au moins), il y a "bataille"
        while (j1.aUneCarte() && j2.aUneCarte() && tas1.sommet().getValeur() == tas2.sommet().getValeur()) {
            System.out.print("Bataille");
            System.out.println(" #<-->#");

            // Deux cartes sont posées retournées
            tas1.empiler(j2.lancer());
            tas2.empiler(j1.lancer());
            faceVisible = !faceVisible;

            // *** Affichage
            affichageActions();
            affichageMainDesJoueurs();
            // *** Affichage

            // Deux cartes sont posées visibles,
            // mais il faut encore s'assurer que les deux joueurs ont des cartes...
            if (j1.aUneCarte() && j2.aUneCarte()) {
                tas1.empiler(j1.lancer());
                tas2.empiler(j2.lancer());
                faceVisible = !faceVisible;

                // *** Affichage
                affichageActions();
                affichageMainDesJoueurs();
                // *** Affichage
            }
            // Et on répètera ça tant qu'il y a "bataille".
        }

        // En sortie de la boucle, il faut analyser la situation pour savoir
        // qui a remporté le pli.
        // Pour l'instant, le gagnant du pli n'est pas encore déterminé.
        gagnant = null;

        // Il faut savoir si le dernier coup de bataille s'est terminé correctement.
        // Car en effet si l'un des deux joueurs n'a pas assez de cartes, il pert.
        // C'est pour cette raison que le booléen faceVisible a été ajouté.
        // Si les cartes sur les tas sont posées faces visibles alors la bataille
        // s'est terminée correctement.
        // Sinon (else) un des deux joueurs (voir les deux) n'a pas pu terminer.
        if (faceVisible) {
            if (tas1.sommet().getValeur() > tas2.sommet().getValeur()) {
                gagnant = j1;
            } else if (tas1.sommet().getValeur() < tas2.sommet().getValeur()) {
                gagnant = j2;
            }
        } else {
            if (j1.aUneCarte()) {
                gagnant = j1;
            } else if (j2.aUneCarte()) {
                gagnant = j2;
            }
        }

        // Et le gagnant ramasse.
        // Remarquez que gagnant peut être null.
        // Il faut prévoir ça dans la méthode ramasse.
        ramasse(gagnant);
    }

    private void gagnant(Joueur j) {
        System.out.println("Bravo " + j.getNom());
    }

    private void gagnant() {
        System.out.println("Match nul");
    }

    private void ramasse(Joueur gagnant) {
        if (gagnant != null) {
            // *** Affichage
            System.out.println(gagnant.getNom() + " remporte le pli");
            // *** Affichage

            // Ramassage aléatoire si les deux tas sont non vides
            while (!tas1.vide() && !tas2.vide()) {
                if (random.nextBoolean()) {
                    gagnant.ranger(tas1.depiler());
                } else {
                    gagnant.ranger(tas2.depiler());
                }
            }

            // Ramasser le tas restant
            while (!tas1.vide()) {
                gagnant.ranger(tas1.depiler());
            }
            while (!tas2.vide()) {
                gagnant.ranger(tas2.depiler());
            }
        } else {
            // *** Affichage
            System.out.println("personne ne remporte le pli");
            // *** Affichage
        }
    }
}
