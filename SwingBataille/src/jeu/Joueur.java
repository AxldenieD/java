package jeu;

import carte.Carte;
import utilitaires.File;

/**
 * @author Y. Maillot
 */
public class Joueur {
    
    // Nom du joueur
    private String nom;
    // Ses cartes en main
    private File main;

    // Constructeur
    public Joueur(String nom) {
        this.nom = nom;
        main = new File(32);
    }

    // Ajout d'une carte à la main du joueur
    public void ranger(Carte c) {
        main.entrer(c);
    }

    // Le joueur sort une carte de sa main
    public Carte lancer() {
        return main.sortir();
    }

    // Indique si le joueur a encore au moins une carte
    public boolean aUneCarte() {
        return !main.vide();
    }

    // Getter pour le nom du joueur
    public String getNom() {
        return nom;
    }

    // Affiche les cartes du joueur
    public void affiche() {
        main.affiche();
    }

    // Retourne le nombre de cartes du joueur
    public int nbCartes() {
        return main.getQuantite();
    }
}
