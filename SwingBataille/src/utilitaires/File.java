package utilitaires;

import carte.Carte;

/**
 * @author Y. Maillot & J. Lepagnot
 */
public class File {
    private Carte[] file;
    private int premier;
    private int dernier;
    private int quantite;

    public File(int n) {
        file = new Carte[n];
        premier = 0;
        dernier = 0;
        quantite = 0;
    }

    public void entrer(Carte c) {
        file[dernier] = c;
        quantite++;
        dernier = (dernier + 1) % file.length;
        /* Cette dernière ligne revient à faire:
        dernier++;
        if (dernier == file.length) {
            dernier = 0;
        }
        */
    }

    public Carte sortir() {
        Carte c = file[premier];
        quantite--;
        premier = (premier + 1) % file.length;
        /* Cette dernière ligne revient à faire:
        premier++;
        if (premier == file.length) {
            premier = 0;
        }
        */
        return c;
    }

    public boolean vide() {
        return quantite == 0;
    }

    public boolean plein() {
        return quantite == file.length;
    }

    public int getQuantite() {
        return quantite;
    }

    public void affiche() {
        if (quantite > 0) {
            System.out.print("{");
            int p = premier;
            do {
                System.out.print(file[p].getFigure() + " ");
                p = (p + 1) % file.length;
            } while(p != dernier);
            System.out.print("}");
        }
    }
}
