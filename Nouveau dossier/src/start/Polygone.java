package start;

public class Polygone {


	private Point[] TabPoint; 
	
	

	
	public Polygone(Point num1, Point num2, Point num3, Point ...val)
	{
		int nb=val.length;
		this.TabPoint = new Point[3+nb];
		this.TabPoint[0]=num1;
		this.TabPoint[1]=num2;
		this.TabPoint[2]=num3;
		for(int i=3;i<3+nb;i++)
		{
			this.TabPoint[i]=val[i-3];
		}
	}

	
	public int RecupNbSommet()
	{
		return TabPoint.length;
	}
	public void ModifPoint(int n, double x, double y)
	{
		TabPoint[n].setX(x);
		TabPoint[n].setY(y);
	}
	
	public void AccedePoint(int n)
	{
	  TabPoint[n].affiche();
	}

	public void AffPolygone()
	{
		int nb=this.RecupNbSommet();
		for(int i=0;i<nb;i++)
		{
			this.AccedePoint(i);
		}
		
	}
	public void translate(double x, double y)
	{
		int nb=this.RecupNbSommet();
		for(int i=0;i<nb;i++)
		{
			this.TabPoint[i].translate(x, y);
		}
	}
	
	
	public void rotation(double ang)
	{
		int nb=this.RecupNbSommet();
		for(int i=0;i<nb;i++)
		{
			this.TabPoint[i].rotation(ang);
		}
		
	}

}