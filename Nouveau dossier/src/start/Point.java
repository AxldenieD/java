package start;

public class Point {
 
public static final double origineX = 0;
public static final double origineY = 0;
public static int nb=0;	
	
private double x, y;
private double r, theta;

 public Point(double x, double y) 
 {
	this.x = x;
	this.y = y;
	this.r = Math.sqrt(x*x+y*y);
	this.theta = Math.atan2(y,x);
	this.nb++;
}
public Point() 
{
 this(0.0, 0.0);
}
public double getX()
{
	return this.x;
}
public double getY()
{
	return this.y;
}
public void setX(double x) 
{
	this.x = x;
	this.r = Math.sqrt(this.x*this.x+this.y*this.y);
	this.theta = Math.atan2(this.y,this.x);
} 
public void setY(double y) 
{
	this.y = y;
	this.r = Math.sqrt(this.x*this.x+this.y*this.y);
	this.theta = Math.atan2(this.y,this.x);
}
public void affiche()
{
System.out.println("X : "+this.x+" Y:"+this.y);
System.out.println("r : "+this.r+" Theta:"+this.theta);	
}

public void rotation(double angle)
{
	this.x=this.r * Math.cos(this.theta + angle);
	setY(this.r * Math.sin(this.theta + angle));
}
public void translate(double Xx, double Yy)
{
	this.x = this.x + Xx;
	this.y = this.y + Yy;
}

}
