package Base;

import java.util.Scanner;

public class ExoTab {
    
    public static void exo1_1(int[] t) {
        for (int e : t) {
            System.out.print(e + " ");
        }
        System.out.println();
    }
    
    public static void exo1_1_oldStyle(int[] t) {
        for (int i = 0; i < t.length; i++) {
            System.out.print(t[i] + " ");
        }
        System.out.println();
    }
    
    public static int[] exo1_2() {
        Scanner lire = new Scanner(System.in);
        System.out.print("Capacité du tableau : ");
        int n = lire.nextInt();
        int[] t = new int[n];
        for (int i = 0; i < n; i++) {
            System.out.print("Valeur de la case " + i + " : ");
            t[i] = lire.nextInt();
        }
        return t;
    }

    public static void main(String[] args) {
        int[] t = {7,4,2,1,5};
        exo1_1(t);
        int[] t2 = exo1_2();
        exo1_1(t2);
    }
}
