/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Base;

import java.util.Scanner;

/**
 *
 * @author alexandre.denieul
 */
public class MiseEnJambeEtRecherche {
	
     public static int[] saisie()
    {//exercice 2
      Scanner lire = new Scanner(System.in);
      int[] tab; 
      int i,lng;  
      System.out.println("entrer la dimension");
      i=lire.nextInt();
      tab = new int[i];
      lng=tab.length;
      for(i=0;i<lng;i++)
     {
      System.out.println("entrer le chifre N°"+(i+1)+":");
      tab[i]=lire.nextInt();
     }
     return tab;
    }
     
     
    public static int PositionPetitMini(int[] t)
    {
     int i,pos=0,mini=t[0],lng=t.length;
     for(i=0;i<lng;i++)
     {
     if(t[i]<mini)
        {//plus petite possition du mini
            mini=t[i];
            pos=i;
        }
     }
     return pos;
    }
    public static int PositionPlusGrandMini(int[] t)
    {//plus grande position du mini
     int i,pos=0,mini=t[0],lng=t.length;
     for(i=0;i<lng;i++)
     {
     if(t[i]<=mini)
        {//plus petite possition du mini
            mini=t[i];
            pos=i;
        }
     }
     return pos;
    }
    public static int RecupMin(int[] t)
    {//exercice 1
       int i,lng,min=t[0];
       lng=t.length;
       for(i=1;i<lng;i++)
       {
        if(t[i]<min)min=t[i];
       }
     return min;
    }
    public static int PosParamTab(int[] t, int val)
    {//exercice 5 et 6
        int i,lng,pos;
        lng=t.length;
        for(i=0;i<lng;i++)
        {
         if (t[i]==val) return i;
        }
      return -1;
    }
    public static int PosMiddleParamTab(int[] t, int val)
    {//exercice 7 - 8 - 9 position mediane de l'occurance !!
     int[] listposition;
     int i,j=0,lng,nb=0;
     lng=t.length;
     for(i=0;i<lng;i++)
     {//on compte le nombre d'occurence de la valeur dans le tableau
         if (t[i]==val)nb++;
     }
     if(nb==0)
     {
    	return -1;
     }
     else
     {
    	 listposition = new int[nb];
         for(i=0;i<lng;i++)
         {
             if (t[i]==val)
             {//on enregistre les diff�rentes positions
                 listposition[j]=i;
                 j++;
             }
         }
         lng = listposition.length;
         System.out.println(lng);
         return (listposition[(lng / 2)]);
     }
    
    }

    public static int PosMedV2(int[] t, int val)
    {
    	int x,i=-1,j=-1,lng;//i repr�sente la position la plus � gauche de val, j la plus � droite (respectivement l'indice le plus petit et l'indice le plus grand)
    	//x variable de boucle
    	int tmpi,tmpj;//variable temporaires permettant de test l'�volution de i et j
    	lng=t.length;
    	if(t==null || lng <=0)
    	{
    	System.out.println("Tableau non valide");
    	return -1;
    	}
    	else
    	{
    		//on r�cup�re i et j;
    		for(x=0;x<lng;x++)
    		{
    			if(t[x]==val)
    				{
    				i=x;
    				break;
    				}
    		}
    		for(x=lng-1;x>=0;x--)
    		{
    			if(t[x]==val)
				{
				j=x;
				break;
				}
    		}
 /*On test les valeurs de i et j pour voir si on peut executerr la suite*/
 if(i==-1 || j==-1)
 {
  System.out.println("valeur absente du tableau ");
  return -1;
 }
 else
 {
  if(i>j)
  {//test du cas o� l'indice le plus petit est sup�rieur au plus grand - cas impossible
 	System.out.println("erreur bizarre ");
    return -1;
  }
  else
  {
  	if(i==j)
  	{//cas ou la valeur est pr�sente qu'une seule fois donc on a fini
  		return i;
  	}
    	else
    	{
    		//on va faire �voluer i et j jusqu'� ce qu'ils soient egaux.
    		tmpi=i;tmpj=j;
    		while(i!=j && i<j)
    		{
    			tmpi++;
    			if(t[tmpi]==val)i=tmpi;
    			tmpj--;
    			if(t[tmpj]==val)j=tmpj;
    		}
    		return i;
    		
    	}
  }
 }
 }
    	
    	
    }
    
    
    
    public static void main(String[] pouet)
    {
    int res,p;
    int[] tab;
    tab=saisie();
    //System.out.println("mini du tab :" + RecupMin(tab));
 /*   p=PositionPetitMini(tab);
    System.out.println("position mini : " + p);
    p=PositionPlusGrandMini(tab);
    System.out.println("grande position mini : " + p);*/
 /*   p=PosParamTab(tab, 4);
    if(p == -1)
    {
      System.out.println("valeur non trouv�e");
    }else System.out.println("valeur trouv�e � la position :" + p);
        p = PosMiddleParamTab(tab,4);
        if(p==-1)
        {
        	System.out.println("valeur absente du tableau");        	
        }
        else  System.out.println("position mediane :" + p);
    }*/
    
    System.out.print("position mediane :");
    System.out.print( PosMedV2(tab, 4));
    }
}
