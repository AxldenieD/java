package Base;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class TriTab {

	/*************************************************/
	/********fonctions bateaux juste utiles***********/
	/*************************************************/
		public static int[] saisie()
	    {//exercice 2
	      Scanner lire = new Scanner(System.in);
	      int[] tab; 
	      int i,lng;  
	      System.out.println("entrer la dimension");
	      i=lire.nextInt();
	      tab = new int[i];
	      lng=tab.length;
	      for(i=0;i<lng;i++)
	     {
	      System.out.println("entrer le chifre N�"+(i+1)+":");
	      tab[i]=lire.nextInt();
	     }
	     return tab;
	    }
		public static int[] genere()
		{//demande la dimension et remplit le talbeaux avec des chiffres al�atoire entre 0 et 100
			   Scanner lire = new Scanner(System.in);
			   Random Nb;
			      int[] tab; 
			      int i,lng;  
			      System.out.println("entrer la dimension");
			      i=lire.nextInt();
			      tab = new int[i];
			      lng=tab.length;
			      for(i=0;i<lng;i++)
			     {
			     Nb = new Random();
			      tab[i]=Nb.nextInt(100);
			     }
			return tab;
		}
		public static void affich(int[] t)
		{
			int i,lng;
			lng=t.length;
			for(i=0;i<lng;i++)
			{
				System.out.println("case : "+i+" valeur :"+t[i]);
			}
		}
		
		/*****************************************************/
		/********Fin fonctions bateaux juste utiles***********/
		/*****************************************************/
	
		
		
	public static int[] TriTabSelection(int[] t)
	{
		int min,i=0,j=0,indice=0,lng=t.length;//i et j variables de boucles, min variable stockant la valeur minimum durant les parcours, indice le num�ro de la case
		//o� a �t� trouv� le minimum pour l'inversion.
		//tri par selection du minimum cela veut dire : on parcour le tableau on met le minimum � la case 1, on reparcours le tableau
		//en partant de la deux�me case, on cherche le minimum on le met case 2, etc ....
		for(i=0;i<lng-1;i++)
		{//on boucle sur tout le tableau (techniquement on devrais boucler avec : '<lng-1')
			min=t[i];//on initialise le minimum 
			indice=i;
			for(j=i;j<lng;j++)
			{//on cherche le minimum dans la portion de tableau i -> fin
				if(t[j]<min)
				{
					min=t[j];
					indice=j;
				}
			}
			//on �change les valeurs des cases test� et la case o� on a trouv� le minimum
			t[indice]=t[i];
			t[i]=min;
		}
		return t;
	}
	public static int[] TriTabInsertion(int[] t)
	{
		//algorithme tri par insertion, en gros on parcours tout le tableau
		//pour chaque case test�, on doit la placer dans les cases qui la pr�c�de
		int i,j,k,lng=t.length,temp;
		for(i=2;i<lng;i++)
		{//boucle de parcours du tableau
			for(j=0;j<i;j++)//on va jusqu'� i inclus car on va devoir d�caler et donc on
			{
				if(t[i]>t[j])//on cherche l'emplacement � ins�rer t[i]
				{//vu que l'on parcours un tableau tri� de fa�on croissante alors on a trouv� la case o� inseerr la valeur
					temp=t[j];
					t[j]=t[i];
					//et l� on d�calle la fin du parcours
					for(k=j+1;k<i;k++)
					{
						t[k]=temp;
						temp=t[k+1];
					}
					break;//on casse le 2�me for pour ne pas boucler plus que necessaire
				}
			}		
			
		}
		return t;
	}
	public static void main(String[] argv)
	{
		long start,duree;//variable me permettant de tester la rapidit� d'execution d'une fonction !
		int[] t;
		t=genere();
	//	affich(t);
		start = System.nanoTime();
	//	t=TriTabSelection(t);
		t=TriTabInsertion(t);
		duree = System.nanoTime() - start;
		affich(t);
		System.out.println("Temps d'�x�cution : "+duree);
	}
}
