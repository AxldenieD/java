package tetris;

import java.util.Random;

public class Bloc {
	private int NbCases;
	private static Random rand=new Random();
	
	public Bloc()
	{
		this.NbCases = rand.nextInt(4)+1;
	}

	public int getNbCases() {
		return NbCases;
	}
	
}
