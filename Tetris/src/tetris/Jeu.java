package tetris;

import java.util.Scanner;

public class Jeu {
	private Colonne[] colonnes;
	private int points;
	public Jeu(int a, int b, int... c)
		{
		int nb=c.length+2;
		this.colonnes = new Colonne[nb];
		this.colonnes[0] = new Colonne(a);
		this.colonnes[1] = new Colonne(b);
		for(int i=2;i<nb;i++)
			{
				this.colonnes[i] = new Colonne(c[i-2]);
			}
		}
	public int getPoint() {
		return points;
	}
	
	public void joueUnCoup() {
		// G�n�ration d�un nouveau bloc
		Bloc b = new Bloc();
		// Pr�sentation du bloc et des colonnes au joueur
		System.out.println("Nouveau bloc de " + b.getNbCases() + " case(s)");
		System.out.println("Dans quelle colonne le placer ?");
		int i = 1;
		for (Colonne c : colonnes) {
		System.out.println("Colonne " + i + " : " + c);
		i++;
		}
		// R�cup�ration du choix du joueur
		Scanner lire = new Scanner(System.in);
		i = lire.nextInt();
		while ((i < 1) || (i > colonnes.length) || !colonnes[i - 1].estRemplissable()) {
		System.out.println("Colonne inexistante ou non remplissable, reessayez !");
		i = lire.nextInt();
		}
		// Ajout du bloc � la colonne choisie
		if (colonnes[i - 1].ajoute(b)) {
		// Ajout d�un point si la colonne se vide
		this.points++;
		}
		// Affichage du nombre de points
		System.out.println("Vous avez " + this.points + " points");
		System.out.println();
		}
	
	public void joueUnePartie()
	{
		
		boolean fini=false,testcolonne=false;//booleen testant si la partie est termin�e
		int nb = this.colonnes.length;
		int i;
		while(!fini)
		{
			testcolonne=false;
			this.joueUnCoup();//on joue un coup et ensuite on v�rifi� si au moins une colonne est remplissable
			i=0;
			while(testcolonne==false && i<nb)
			{
				testcolonne = this.colonnes[i].estRemplissable();
				i++;
			}
			fini=!testcolonne;
			
		}
	System.out.println("le jeu est termin� !");
		
		
	}
	
}

