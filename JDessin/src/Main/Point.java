package Main;

public class Point {
	
	protected int x,y;
	
	public Point( int x , int y)
	{
		this.x=x;
		this.y=y;
	}
	
	public void translateX(int nb)
	{
		this.x+=nb;
	}
	public void translateY(int nb)
	{
		this.y+=nb;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}
	
	
	

}
