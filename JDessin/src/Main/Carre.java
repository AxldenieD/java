package Main;

import java.awt.Graphics;

public class Carre {
	
	protected Point a,b,c,d;
	protected int Lng;
	protected Graphics g;

	public Carre(int x, int y, int lng)
	{
		
//		this.g = new Graphics();
		
		
		/*
		 *     a---------b
		 *     |         |
		 *     |         |
		 *     |         |
		 *     c---------d
		 * 
		 * */
		this.Lng=lng;
		 this.a=new Point(x,y);
		 this.b=new Point(x+lng,y);
		 this.c=new Point(x,y+lng);
		 this.d=new Point(x+lng,y+lng);
	}
	
	
	
	
	public int getLng() {
		return Lng;
	}




	public void setLng(int lng) {
		Lng = lng;
	}




	public void translate(int Xcoef, int Ycoef)
	{
		this.a.translateX(Xcoef);
		this.a.translateY(Ycoef);
		
		this.b.translateX(Xcoef);
		this.b.translateY(Ycoef);
		
		this.c.translateX(Xcoef);
		this.c.translateY(Ycoef);
		
		this.d.translateX(Xcoef);
		this.d.translateY(Ycoef);
	}
	public void draw()
	{
		g.fillRect(10,10,80,80);
	}

}
