package Main;
import java.awt.*;
import javax.swing.*;


public class JCanvas extends JPanel {
	
	protected Carre c;
	
	public JCanvas(int x,int y,int lng)
	{
		this.c = new Carre(x,y,lng);
	}
	
	public void paint(Graphics g) {
	/*	Color col = g.getColor();*/
		g.setColor(Color.RED);
		g.fillRect(10,10, 50, 50);
		g.fillRect(this.c.a.getX(),this.c.a.getY(),this.c.getLng(),this.c.getLng());
		g.setColor(Color.BLUE);
		g.fillOval(150,50,80,80);
	//	g.setColor(c);
		
	}
	
	public void Tr(int x, int y)
	{
		this.c.translate(x, y);
	}
}
