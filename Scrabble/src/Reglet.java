
public class Reglet {
	private Jeton[] reg;//tableau de plusieurs jetons soit 7 lettres maximum
	private int n; //nombre de jetons pr�sent dans le reglet
	
	public Reglet()
	{
		this.n=0;
		this.reg = new Jeton[7];
	}
	
	/************************ Zone priv�e ***************************************/
	private int Position(char c)
	{
		int i=0;
		while(i<this.n)
		{
			if (this.reg[i].GetLettre() == c) return i;
			i++;
		}
		return -1;
	}
	public void SetReg(Jeton[] r)
	{
		for(int i = 0 ;i<7;i++)
		{
			this.reg[i]=r[i];
		}
	}
	public Jeton[] GetReg()
	{
		return this.reg;
	}
	public int getnb()
	{
		return this.n;
	}
	public void setnb(int n)
	{
		this.n=n;
		
	}
	public int getNbJeton()
	{
		
		return this.reg[0].GetNbJetons();
	}
	/************************ Fin Zone priv�e ***********************************/
	
	/****************************************************************************/
	/****																	*****/
	/****																	*****/
	/****		Zone entre deux (�a sert � rien mais j'aime bien)			*****/
	/****																	*****/
	/****																	*****/
	/************************ Zone publique *************************************/
	
	public Reglet clone()
	{
		Reglet t = new Reglet();
		for(int i=0;i<this.n;i++)
		{
			t.Ajoute(this.reg[i].GetLettre());
		}
		return t;
	}
	public void Affiche()
	{
		for(int i=0; i<this.n;i++)
		{
			this.reg[i].Affiche();
		}
	}
	
	public boolean Retire(char c)
	{
		int p=this.Position(c);
		if(p != -1)
		{
			this.n--;
			this.reg[p]=this.reg[n];
			this.reg[n]=null;//pas obligatoire !
			return true;
		}
		else
		{
			return false;
		}
		
	}
	public void Ajoute(char c)
	{
		
		if (this.n<7 && (c>='A' && c<='Z'))
			{
			this.reg[this.n]=new Jeton(c);
			this.n++;
			}
		else System.out.println("reglet plein ! ou caract�re minuscule");
	}
	
	public boolean Secritv2(String s)
	{
		Reglet tempR = new Reglet();
		tempR=this.clone();
		
		
		for(int i=0;i< s.length();i++)
		{
			
			if (!tempR.Retire(s.charAt(i)))
			{
				return false;
			}
		}
		return true;
	}
	
	public boolean Secrit(String s)
	{
		Reglet tempR = new Reglet();
		tempR=this.clone();
		
		int nb=s.length();
		for(int i=0; i<nb;i++)
		{//on parcours toutes les lettres de la chaine de caract�re.
			boolean t=false;
			boolean ajoute=true; //variable qi va servir � ne supprimer une lettre Qu'une fois d'un reglet
			//on doit v�rifier si chaque caract�re est pr�sent dans le reglet
			for(int j=0; j<tempR.n;j++)
			{
				if(tempR.reg[j].GetLettre()== s.charAt(i) && ajoute==true)
					{
					 t=true;
					 tempR.Retire(s.charAt(i));
					 ajoute=false;
					}
			}
			if(t==false)return false; 
		}
	 return true;
	}
}
