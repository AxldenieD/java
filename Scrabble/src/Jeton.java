
public class Jeton {

	private char lettre;
	private static int nbJetons=0;
	
	/***** place constructeur ******/
	public Jeton(char a)
	{
		this.lettre=a;
		nbJetons++;
	}
	public Jeton()
	{//surcharge constructeur jeton
		this('*');
	}
	/******fin constructeur *********/
	public char GetLettre()
	{
		return this.lettre;
	}

	public int GetNbJetons()
	{
		return Jeton.nbJetons;		
	}
	public void Affiche()
	{
		System.out.println("["+this.lettre+"]");
	}
}
