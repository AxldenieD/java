package heritageABC;

public class Main {
	
	public static void main(String[] argv)
	{
		A t = new A();
		B tb = new B();
		C tc = new C();
		
		A.affiche(t);
		B.affiche(tb);
		C.affiche(tc);
	}
}
