package Step;

public class CompteurObjet {
//Exercice 1 sur les objets.
	private static int nombre=0;//nombre est d�clar� static, donc il est commun � toutes les instances de Compteur Objet
	
	public CompteurObjet()
	{
		CompteurObjet.nombre++;//accession � un objet static ! on utilise pas this car il ne d�pend pas de l'instance (autre �criture accept�e : CompteurObjet.nombre++; )
	}
	public void aff()
	{
		System.out.println(nombre);
		
	}
}
