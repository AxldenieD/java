package Step;

import start.Point;

public class Triangle {
	
	private Point p1;
	private Point p2;
	private Point p3;
	
	public Triangle(Point p1, Point p2, Point p3)
	{
		this.p1 = p1;
		this.p2 = p2;
		this.p3 = p3;
	}
	public Triangle()
	{
		this(new Point(),new Point(), new Point());
	}
	public void Affiche()
	{
		p1.affiche();
		p2.affiche();
		p3.affiche();
	}
	public void translate(double valX, double valY)
	{
		this.p1.setX(p1.getX() + valX);
		this.p1.setY(p1.getY() + valY);
		this.p2.setX(p2.getX() + valX);
		this.p2.setY(p2.getY() + valY);
		this.p3.setX(p3.getX() + valX);
		this.p3.setY(p3.getY() + valY);
	}
	public void rotation(double angle)
	{
		this.p1.rotation(angle);
		this.p2.rotation(angle);
		this.p3.rotation(angle);
	}

}
