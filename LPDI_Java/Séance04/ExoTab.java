package tableaux;

import java.util.Scanner;

public class ExoTab {
    
    public static void exo1_1(int[] t) {
        for (int e : t) {
            System.out.print(e + " ");
        }
        System.out.println();
    }
    
    public static void exo1_1_oldStyle(int[] t) {
        for (int i = 0; i < t.length; i++) {
            System.out.print(t[i] + " ");
        }
        System.out.println();
    }
    
    public static int[] exo1_2() {
        Scanner lire = new Scanner(System.in);
        System.out.print("Capacité du tableau : ");
        int n = lire.nextInt();
        int[] t = new int[n];
        for (int i = 0; i < n; i++) {
            System.out.print("Valeur de la case " + i + " : ");
            t[i] = lire.nextInt();
        }
        return t;
    }

    public static int exo2_1(int[] t) {
        if ((t == null) || (t.length <= 0)) {
            return -1;
        }
        int e = t[0];
        for (int j : t) {
            if (j < e) {
                e = j;
            }
        }
        return e;
    }

    public static int exo2_3(int[] t) {
        if ((t == null) || (t.length <= 0)) {
            return -1;
        }
        int i = 0;
        for (int j = 1; j < t.length; j++) {
            if (t[j] < t[i]) {
                i = j;
            }
        }
        return i;
    }

    public static int exo2_4(int[] t) {
        if ((t == null) || (t.length <= 0)) {
            return -1;
        }
        int i = 0;
        for (int j = 1; j < t.length; j++) {
            if (t[j] >= t[i]) {
                i = j;
            }
        }
        return i;
    }

    public static int exo2_5(int e, int[] t) {
        int i = 0;
        while(t[i] != e) {
            i++;
        }
        return i;
    }
    
    public static int exo2_6(int e, int[] t) {
        if ((t == null) || (t.length <= 0)) {
            return -1;
        }
        int i = 0;
        while (i < t.length && t[i] != e) {
            i++;
        }
        if (i == t.length) {
            return -1;
        }
        else {
            return i;
        }
    }

    public static int premier(int e, int[] t) {
        int i = 0;
        while ((i < t.length) && (t[i] != e))
            i++;
        return i;
    }

    public static int dernier(int e, int[] t) {
        int j = t.length - 1;
        while ((j >= 0) && (t[j] != e))
            j--;
        return j;
    }

    public static int suivant(int i, int e, int[] t) {
        i++;
        while ((i < t.length) && (t[i] != e))
            i++;
        return i;
    }

    public static int precedent(int j, int e, int[] t) {
        j--;
        while ((j >= 0) && (t[j] != e))
            j--;
        return j;
    }
    
    public static int exo2_7(int e, int[] t) {
        if ((t == null) || (t.length <= 0)) {
            return -1;
        }
	int i = premier(e, t);
	int j = dernier(e, t);
	while(i < j) {
		i = suivant(i, e, t);
		j = precedent(j, e, t);
	}
	return j;
    }
    
    public static void main(String[] args) {
        int[] t = {7,4,2,1,5,7,6,1,1};
        /*
        exo1_1(t);
        int[] t2 = exo1_2();
        exo1_1(t2);
        */
        /*
        System.out.println("Plus petit élément : " + exo2_1(t));
        System.out.println("Plus petit indice du plus petit élément : " + exo2_3(t));
        System.out.println("Plus grand indice du plus grand élément : " + exo2_4(t));
        System.out.println("Position de 4 : " + exo2_5(4, t));
        System.out.println("Position de 8 : " + exo2_6(8, t));
        */
        System.out.println("Position centrale de 1 : " + exo2_7(8, t));
    }
}
