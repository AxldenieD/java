package scrabble;

/**
 * @author yvan
 */
public class Jeton {
    private char lettre;
    private int valeur;
    private static int valeurLettreFrancaise[] = {1,2,2,2,1,4,2,4,1,8,10,1,2,1,1,2,8,1,1,1,1,4,10,10,10,10};

    public Jeton(char lettre, int valeur) {
        this.lettre = lettre;
        this.valeur = valeur;
    }

    public Jeton() {
        this('*', 0);
    }

    public Jeton(char lettre) {
        this(lettre, valeurLettreFrancaise[lettre-'A']);
    }

    public char getLettre() {
        return lettre;
    }

    public int getValeur() {
        return valeur;
    }

    public void affiche() {
        System.out.print("["+lettre+"]");
    }
}
