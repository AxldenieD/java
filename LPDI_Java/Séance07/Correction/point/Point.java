package point;

public class Point {

    // ATTRIBUTS
    private double x, y;
    private double module, theta;

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
        cartesienPolaire();
    }

    public Point() {
        this(0.0, 0.0);
    }

    // METHODES PRIVEES
    private void cartesienPolaire() {
        module = Math.sqrt(x * x + y * y);
        theta = Math.atan2(y, x);
    }

    private void polaireCartesien() {
        x = module * Math.cos(theta);
        y = module * Math.sin(theta);
    }

    // ACCESSEURS
    public double getModule() {
        return module;
    }

    public void setModule(double module) {
        this.module = module;
        polaireCartesien();
    }

    public double getTheta() {
        return theta;
    }

    public void setTheta(double theta) {
        this.theta = theta;
        polaireCartesien();
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
        cartesienPolaire();
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
        cartesienPolaire();
    }

    // AUTRES METHODES PUBLIQUES
    public void rotation(double angle) {
        theta += angle;
        polaireCartesien();
    }

    public void translation(double dx, double dy) {
        x += dx;
        y += dy;
        cartesienPolaire();
    }
}
