/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sudoku;

import java.util.Arrays;

/**
 *
 * @author maillot
 */
public class Sudoku {

    private Case[][] grille;

    public Sudoku(Triplet... ltriplet) throws Exception {
        grille = new Case[9][9];
        for (int l = 0; l < grille.length; l++) {
            for (int c = 0; c < grille[0].length; c++) {
                grille[l][c] = new Case();
            }
        }

        for (Triplet t : ltriplet) {
            grille[t.l][t.c] = new Case(t.k);
        }
    }

    public void setCase(int l, int c, char k) throws Exception {
        grille[l][c].setChiffre(k);
    }

    public char getCase(int l, int c) {
        return grille[l][c].getChiffre();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Sudoku other = (Sudoku) obj;
        if (!Arrays.deepEquals(this.grille, other.grille)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 23 * hash + Arrays.deepHashCode(this.grille);
        return hash;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (int l = 0; l < 3; l++) {
            for (int c = 0; c < 3; c++) {
                sb.append(grille[l][c]);
            }
            sb.append(' ');
            for (int c = 3; c < 6; c++) {
                sb.append(grille[l][c]);
            }
            sb.append(' ');
            for (int c = 6; c < 9; c++) {
                sb.append(grille[l][c]);
            }
            sb.append(' ');
            sb.append('\n');
        }
        sb.append('\n');
        for (int l = 3; l < 6; l++) {
            for (int c = 0; c < 3; c++) {
                sb.append(grille[l][c]);
            }
            sb.append(' ');
            for (int c = 3; c < 6; c++) {
                sb.append(grille[l][c]);
            }
            sb.append(' ');
            for (int c = 6; c < 9; c++) {
                sb.append(grille[l][c]);
            }
            sb.append(' ');
            sb.append('\n');
        }
        sb.append('\n');
        for (int l = 6; l < 9; l++) {
            for (int c = 0; c < 3; c++) {
                sb.append(grille[l][c]);
            }
            sb.append(' ');
            for (int c = 3; c < 6; c++) {
                sb.append(grille[l][c]);
            }
            sb.append(' ');
            for (int c = 6; c < 9; c++) {
                sb.append(grille[l][c]);
            }
            sb.append(' ');
            sb.append('\n');
        }
        sb.append('\n');
        return sb.toString();
    }

    /*
    public void affiche() {
    for(int l = 0; l < 3; l++) {
    for(int c = 0; c < 3; c++) {
    System.out.print(grille[l][c]);
    }
    System.out.print(' ');
    for(int c = 3; c < 6; c++) {
    System.out.print(grille[l][c]);
    }
    System.out.print(' ');
    for(int c = 6; c < 9; c++) {
    System.out.print(grille[l][c]);
    }
    System.out.println();
    }
    System.out.print('\n');
    for(int l = 3; l < 6; l++) {
    for(int c = 0; c < 3; c++) {
    System.out.print(grille[l][c]);
    }
    System.out.print(' ');
    for(int c = 3; c < 6; c++) {
    System.out.print(grille[l][c]);
    }
    System.out.print(' ');
    for(int c = 6; c < 9; c++) {
    System.out.print(grille[l][c]);
    }
    System.out.print(' ');
    System.out.println();
    }
    System.out.print('\n');
    for(int l = 6; l < 9; l++) {
    for(int c = 0; c < 3; c++) {
    System.out.print(grille[l][c]);
    }
    System.out.print(' ');
    for(int c = 3; c < 6; c++) {
    System.out.print(grille[l][c]);
    }
    System.out.print(' ');
    for(int c = 6; c < 9; c++) {
    System.out.print(grille[l][c]);
    }
    System.out.println();
    }
    System.out.println();
    }    */
    public void affiche() {
        int c;
        int l = 0;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                c = 0;
                for (int k = 0; k < 3; k++) {
                    for (int m = 0; m < 3; m++) {
                        System.out.print(grille[l][c++]);
                    }
                    System.out.print(' ');
                }
                l++;
                System.out.println();
            }
            System.out.println();
        }
    }

public boolean ligneValide(int l) {
    for(int c = 0; c < 9; c++) {
        if (grille[l][c].getChiffre() != ' ') {
            for(int k = c+1; k < 9; k++) {
                if (grille[l][c].equals(grille[l][k]))
                    return false;
            }
        }
    }
    return true;
}

    public static void main(String[] args) throws Exception {
        Sudoku sudo = new Sudoku(
                new Triplet(0, 1, '1'),
                new Triplet(0, 6, '6'),
                new Triplet(0, 8, '8'),
                new Triplet(1, 0, '2'),
                new Triplet(1, 1, '6'),
                new Triplet(1, 3, '7'),
                new Triplet(1, 4, '3'),
                new Triplet(2, 1, '3'),
                new Triplet(2, 2, '9'),
                new Triplet(2, 3, '6'),
                new Triplet(2, 7, '5'),
                new Triplet(2, 8, '7'),
                new Triplet(3, 3, '2'),
                new Triplet(3, 4, '4'),
                new Triplet(3, 7, '6'),
                new Triplet(4, 1, '4'),
                new Triplet(4, 3, '9'),
                new Triplet(4, 5, '7'),
                new Triplet(4, 7, '2'),
                new Triplet(5, 1, '8'),
                new Triplet(5, 4, '6'),
                new Triplet(5, 5, '1'),
                new Triplet(6, 0, '7'),
                new Triplet(6, 1, '5'),
                new Triplet(6, 5, '9'),
                new Triplet(6, 6, '3'),
                new Triplet(6, 7, '1'),
                new Triplet(7, 4, '8'),
                new Triplet(7, 5, '6'),
                new Triplet(7, 7, '8'),
                new Triplet(7, 8, '2'),
                new Triplet(8, 0, '1'),
                new Triplet(8, 2, '8'),
                new Triplet(8, 7, '9'));

        //System.out.println(sudo);
        sudo.affiche();
        
        for(int l = 0; l < 9; l++)
            System.out.println(sudo.ligneValide(l));
        
        sudo.setCase(1, 5, '7');
        sudo.affiche();
        System.out.println(sudo.ligneValide(1));
    }
}
