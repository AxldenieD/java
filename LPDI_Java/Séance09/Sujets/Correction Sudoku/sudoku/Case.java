/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sudoku;


/**
 *
 * @author maillot
 */
public class Case {
    private final boolean figée;
    private char chiffre;

    public Case(char chiffre) throws Exception {
        if (chiffre < '1' || chiffre > '9') 
            throw new Exception("Chiffre incorrect");
        this.figée = true;       
        this.chiffre = chiffre;
    }
    
    public Case() {
        this.figée = false;
        this.chiffre = ' ';
    }

    public char getChiffre() {
        return chiffre;
    }

    public void setChiffre(char chiffre) throws Exception {
        if (chiffre < '0' || chiffre > '9') 
            throw new Exception("Chiffre incorrect");
        if (figée) throw 
                new Exception("Changement d'une case figée");
        this.chiffre = chiffre;
    }
    
    public void clear() {
        chiffre = ' ';
    }
    

    @Override
    public String toString() {
        if (figée)
            return "[" + chiffre + "]";
        else
            return "(" + chiffre + ")";
    }
    
    public boolean estRemplie() {
        return chiffre != ' ';
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Case other = (Case) obj;
        if (this.chiffre != other.chiffre) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 89 * hash + this.chiffre;
        return hash;
    }
    
    
    
}
