package tetris;

public class Colonne {
    private int capacite;
    private Bloc[] blocs;
    private int nbBlocs;
    private int nbCases;
    
    public Colonne(int capacite) {
        if (capacite < 4) {
            capacite = 4;
        }
        if (capacite > 9) {
            capacite = 9;
        }
        this.capacite = capacite;
        vide();
    }
    
    private void vide() {
        blocs = new Bloc[capacite];
        nbBlocs = 0;
        nbCases = 0;
    }
    
    public int getCapacite() {
        return capacite;
    }
    
    public int getNbCases() {
        return nbCases;
    }
    
    public boolean ajoute(Bloc b) {
        if (nbBlocs < capacite) {
            blocs[nbBlocs++] = b;
        }
        nbCases += b.getNbCases();
        if (nbCases == capacite) {
            vide();
            return true;
        }
        return false;
    }
    
    public boolean estRemplissable() {
        return nbCases < capacite;
    }
    
    @Override
    public String toString() {
        String c = "";
        for (int i = 0; i < nbBlocs; i++) {
            for (int j = 0; j < blocs[i].getNbCases(); j++) {
                c += i + 1;
            }
        }
        for (int i = nbCases; i < capacite; i++) {
            c += '-';
        }
        return c;
    }
}
