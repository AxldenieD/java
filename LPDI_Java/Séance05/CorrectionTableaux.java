package tableaux;

import java.util.Random;

public class CorrectionTableaux {
    
    // Exo 3.1
    public static void inverser(int[] t) {
        int i, j;
        for(i = 0, j = t.length-1; i < j; i++, j--) {
            int a;
            a = t[i];
            t[i] = t[j];
            t[j] = a;
        }
    }

    // Exo 3.2
    public static int[] insere(int[] t, int e, int p) {
        if (t == null) {
            t = new int[0];
        }
        int[] r;

        if (p > t.length) {
            p = t.length;
        }
        r = new int[t.length + 1];

        // Cas général
        int i;
        for (i = 0; i < p; i++) {
            r[i] = t[i];
        }
        r[i] = e;
        for (int j = i + 1; j < r.length; j++) {
            r[j] = t[j - 1];
        }

        return r;
    }
    
    // Exo 3.3
    public static int[] insereDansTrie(int[] t, int e) {
        if (t == null) {
            t = new int[0];
        }
        int i = 0;
        while (i < t.length && t[i] < e) {
            i++;
        }
        return insere(t, e, i);
    }
    
    // Exo 3.4
    public static int[] interclassement(int[] t1, int[] t2) {
        int[] r = new int[t1.length + t2.length];
        int i = 0, j = 0, k = 0;
        while (i < t1.length && j < t2.length) {
            if (t1[i] < t2[j]) {
                r[k] = t1[i];
                k++;
                i++;
            } else {
                r[k] = t2[j];
                k++;
                j++;
            }
        }

        for (; i < t1.length; i++) {
            r[k] = t1[i];
            k++;
        }

        for (; j < t2.length; j++) {
            r[k] = t2[j];
            k++;
        }

        return r;
    }
    
    // Méthode d'affichage d'un tableau à 1 dimension
    public static void affichage(int[] t) {
        for (int x : t) {
            System.out.print(x + " ");
        }
        System.out.println("");
    }

    // Test de l'exo 3.1
    public static void testInversion() {
        Random r = new Random();
        int[] t = new int[r.nextInt(20) + 10];
        for (int i = 0; i < t.length; i++) {
            t[i] = i + 1;
        }
        affichage(t);

        inverser(t);
        affichage(t);
    }

    // Test de l'exo 3.2
    public static void testInsere() {
        Random r = new Random();
        int[] u = {1, 2, 3};
        int[] v;

        affichage(u);
        for (int i = -3; i < 10; i++) {
            v = insere(u, 10, i);
            affichage(v);
        }

        v = null;
        for (int i = 0; i < 10; i++) {
            v = insere(v, i, r.nextInt(10) - 5);
            affichage(v);
        }
    }
    
    /**
     * Teste si le tableau t est trié, affiche OK si trié ou
     * NOK et le lieu du problème si non trié.
     * @param t Le tableau qui devrait être trié.
     */
    public static void testEstTrie(int[] t) {
        int i = 1;
        while (i < t.length && t[i] >= t[i - 1]) {
            i++;
        }
        System.out.println(i == t.length ? "OK" : "NOK -> " + i);
    }

    // Test de l'exo 3.3
    public static void testInsereDansTrie() {
        Random r = new Random();
        int[] v;
        v = null;
        int n = r.nextInt(50) + 50;
        for (int i = 0; i < n; i++) {
            v = insereDansTrie(v, r.nextInt(1000));
        }
        affichage(v);
        testEstTrie(v);
    }

    // Test de l'exo 3.4
    public static void testInterclassement() {
        Random r = new Random();
        int[] v;
        v = null;
        int n = r.nextInt(50) + 50;
        for (int i = 0; i < n; i++) {
            v = insereDansTrie(v, r.nextInt(1000));
        }
        
        int[] w = null;
        n = r.nextInt(50) + 50;
        for (int i = 0; i < n; i++) {
            w = insereDansTrie(w, r.nextInt(1000));
        }
        
        v = interclassement(v, w);
        affichage(v);
        testEstTrie(v);
    }

    public static void main(String[] args) {
        System.out.println("------- Test inversion -------- ");
        testInversion();
        System.out.println("------- Test insertion -------- ");
        testInsere();
        System.out.println("------- Test insertion dans trié -------- ");
        testInsereDansTrie();
        System.out.println("------- Test interclassement -------- ");
        testInterclassement();
    }
}
