package debut;

/**
 * @author Julien Lepagnot
 */
public class Exo5 {

    public static int lireDepart() {
        System.out.println("Entrer l'horaire de départ :");
        System.out.println("Heure :");
        int h = Exo1a4.exo1(0, 23);
        System.out.println("Minute :");
        int m = Exo1a4.exo1(0, 59);
        int s = Exo1a4.exo2(h, m);
        return s;
    }

    public static int lireDuree() {
        System.out.println("Entrer la durée :");
        System.out.println("Heures :");
        int h = Exo1a4.exo1(0, 2000); // Integer.MAX_VALUE
        System.out.println("Minutes :");
        int m = Exo1a4.exo1(0, 59);
        int s = Exo1a4.exo2(h, m);
        return s;
    }

    public static void afficheArrivee(int s) {
        int h = (s / 3600) % 24;
        int m = (s / 60) % 60;
        System.out.print("Arrivée : ");
        Exo1a4.exo4(h, m);
    }

    public static void main(String[] args) {
        int h = lireDepart();
        int d = lireDuree();
        afficheArrivee(h + d);
    }
}
