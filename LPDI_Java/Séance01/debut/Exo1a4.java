package debut;

import java.util.Scanner;

/**
 * @author Julien Lepagnot
 */
public class Exo1a4 {

    public static int exo1(int m, int M) {
        Scanner lire = new Scanner(System.in);
        int a;
        do {
            System.out.print("Entrer un entier entre " + m + " et " + M + " : ");
            a = lire.nextInt();
        } while ((a < m) || (a > M));
        return a;
    }

    public static int exo2(int h, int m) {
        return h * 3600 + m * 60;
    }

    public static int exo3(int h1, int m1, int h2, int m2) {
        int t1 = exo2(h1, m1);
        int t2 = exo2(h2, m2);
        // return t2 - t1;
        if (t1 < t2) {
            return 1;
        }
        if (t1 > t2) {
            return -1;
        }
        return 0;
    }

    public static void exo4(int h, int m) {
        System.out.println(h + " h " + m);
    }
}
