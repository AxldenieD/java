package debut;

/**
 * Écrire une procédure à un paramètre entier n qui dessine un carré plein de nn*
 * @author yvan.maillot
 */
public class Exo7a11 {
    public static void carré(int n) {
        for(int i = 0; i < n; i++) {
            // Afficher une ligne de n *
            Exo6.ligne('*', '*', '*', n-2);
            System.out.println();
        }
    }

    public static void rectangle(int h, int l) {
        for(int i = 0; i < h; i++) {
            // Afficher une ligne de l *
            Exo6.ligne('*', '*', '*', l-2);
            System.out.println();
        }
    }

    /**
     * Affiche une ligne de n c
     * @param n le nombre de caractères à répéter
     * @param c le caractères à répéter
     */
    public static void ligne(int n, char c) {
        for (int i = 0; i < n; i++) {
            System.out.print(c);
        }
    }

    /**
     * Dessine un triangle isocele plein d'* de hauteur h<hr>
     * Par exemple, triangleIsocele(4) donnerait :<hr>
     * <PRE>
     *    *<br>    3 espaces et 1 *
     *   ***<br>   2 espaces et 3 *
     *  *****<br>  1 espaces et 5 *
     * *******<hr> 0 espaces et 7 *
     * </PRE>
     * @param h la hauteur du triangle
     */
    public static void triangleIsocele(int h) {
        /* Il y a h lignes, toutes différentes, mais dont le nombre d'espaces et
         * et d'* se calculent facilement en fonction du numéro de ligne.
         * Si l'on commence à 0 jusqu'à h-1 (pour i de 0 à h-1
         * Il y (h-i-1) espaces et (2i+1) *
         * On peut faire des boucles, mais on peut aussi se faciliter la vie
         * en écrivant une fonction ligne
         */
        for (int i = 0; i < h; i++) {
            ligne(h - i - 1, ' ');
            ligne(2 * i + 1, '*');
            System.out.println();
        }
    }

    /**
     * Dessine un carré vide de nxn *
     * @param n hauteur du carré
     */
    public static void carreVide(int n) {
        // Il faut traiter les deux lignes extrêmes à part
        Exo6.ligne('*', '*', '*', n - 2);
        System.out.println();
        for (int i = 0; i < n-2; i++) {
            // Afficher une ligne de n *
            Exo6.ligne('*', ' ', '*', n - 2);
            System.out.println();
        }
        Exo6.ligne('*', '*', '*', n - 2);
        System.out.println();
    }

    /**
     * Dessine un rectangle vide de hxl *
     * @param h hauteur du rectangle
     * @param l largeur du rectangle
     */
    public static void rectangleVide(int h, int l) {
        // Il faut traiter les deux lignes extrêmes à part
        Exo6.ligne('*', '*', '*', l - 2);
        System.out.println();
        for (int i = 0; i < h - 2; i++) {
            Exo6.ligne('*', ' ', '*', l - 2);
            System.out.println();
        }
        Exo6.ligne('*', '*', '*', l - 2);
        System.out.println();
    }

    /**
     * Dessine un triangle isocele vide d'* de hauteur h<hr>
     * Par exemple, triangleIsocele(4) donnerait :<hr>
     * <PRE>
     *    *<br>
     *   * *<br>
     *  *   *<br>
     * *******<hr>
     * </PRE>
     * @param h la hauteur du triangle
     */
    public static void triangleIsoceleVide(int h) {
        ligne(h-1, ' ');
        System.out.println("*");

        for (int i = 1; i < h-1; i++) {
            ligne(h - i - 1, ' ');
            Exo6.ligne('*', ' ', '*', 2 * i - 1);
            System.out.println();
        }
        ligne(2*h-1, '*');
        System.out.println();
    }

    private static void grandeLigne(char c1, char c2, int N, int n) {
        for(int i = 0; i < N; i++) {
            ligne(n, c1);
            char a = c1;
            c1 = c2;
            c2 = a;
        }
        System.out.println();
    }

    private static void grosseLigne(char c1, char c2, int N, int n) {
        for(int i = 0; i < n; i++) {
            grandeLigne(c1, c2, N, n);
        }
    }

    /**
     * Damier de NxN carrés de nxn de côté
     * @param N
     * @param n
     */
    public static void damier(int N, int n) {
        char c1 = 'X';
        char c2 = ' ';
        for(int i = 0; i < N; i++) {
            grosseLigne(c1, c2, N, n);
            char a = c1;
            c1 = c2;
            c2 = a;
        }
    }

    public static void main(String[] args) {
        System.out.println("carré plein de 5x5 *");
        carré(5);
        System.out.println("rectangle de 4x10 *");
        rectangle(4, 10);
        System.out.println("triangle isocele de hauteur 10 *");
        triangleIsocele(10);
        System.out.println("carre vide de 5x5 *");
        carreVide(5);
        System.out.println("rectangle vide de 4x10 *");
        rectangleVide(4, 10);
        System.out.println("triangle isocele vide de hauteur 10 *");
        triangleIsoceleVide(10);
        damier(4, 3);

    }
}
