package debut;

/**
 *
 * @author yvan.maillot
 */
public class Exo6 {
    public static void ligne(char g, char m, char d, int n) {
        System.out.print(g);
        for(int i = 0; i < n; i++) {
            System.out.print(m);
        }
        System.out.print(d);
    }

    public static void main(String[] args) {
        ligne('*', ' ', '*', 5);
        System.out.println();
    }
}
