package exo1;

/**
 * @author yvan.maillot
 */
public class CompteurDObjets {
    private static int nbInstances = 0;

    public CompteurDObjets() {
        nbInstances++;
    }

    public static int getNbInstances() {
        return nbInstances;
    }
}
