package debut;

import java.util.Scanner;

/**
 * @author Maillot
 */
public class Exo12a17 {

    /**
     * Retourne n!, c'est-à-dire 1*2*3*...*n
     * @param n valeur dont on veut la factorielle
     * @return n!
     */
    public static int fact(int n) {
        int fi = 1;
        for (int i = 2; i <= n; i++) {
            //fi = fi * i;
            fi *= i;
        }
        return fi;
    }

    /**
     * Retourne x^n
     * @param x
     * @param n
     * @return x^n
     */
    public static double puissance(double x, int n) {
        double xi = 1;
        for (int i = 0; i < n; i++) {
            xi *= x;
        }
        return xi;
    }

    public static void affichagePuissance(double x, int n) {
        System.out.println(x + "^" + n + " = " + puissance(x, n));
    }

    /**
     * Retourne m x (m+1) x (m+2) x ... n
     * Si m > n, retourne 1
     * Si m = n, retourne m (ou n)
     * @param m
     * @param n
     * @return m x (m+1) x (m+2) x ... n
     */
    public static int produit(int m, int n) {
        int r = 1;
        for (int i = m; i <= n; i++) {
            r *= i;
        }
        return r;
    }

    /**
     * Retourne Cnp, le nombre de combinaisons de p parmi n
     * @param n
     * @param p
     * @return
     */
    public static int c(int n, int p) {
        return fact(n) / (fact(p) * fact(n - p));
    }

    public static int cmieux(int n, int p) {
        if (p > n - p) {
            return produit(p + 1, n) / fact(n - p);
        } else {
            return produit(n - p + 1, n) / fact(p);
        }
    }

    public static void affichagecnp(int n, int p) {
        System.out.println("C(" + n + "," + p + ") = " + c(n, p));
        System.out.println("C(" + n + "," + p + ") = " + cmieux(n, p));
    }

    /**
     * Retourne exp(x) à l'ordre n
     * @param x
     * @param n
     */
    public static double exp(double x, int n) {
        double ei = 1;
        for (int i = 1; i <= n; i++) {
            System.out.println(x + "^" + i + " = " + puissance(x, i));
            ei += puissance(x, i) / fact(i);
        }
        return ei;
    }

    public static double expmieux(double x, int n) {
        double ei = 1;
        double xi = 1;
        int fi = 1;
        for (int i = 1; i <= n; i++) {
            xi *= x;
            fi *= i;
            ei += xi / fi;
        }
        return ei;
    }

    public static void affichageexp(double x, int n) {
        System.out.println("exp(" + x + ", " + n + ") = " + exp(x, n));
        System.out.println("exp(" + x + ", " + n + ") = " + expmieux(x, n));
        System.out.println("ref : " + Math.exp(x));
    }

    public static void devel(int n) {
        System.out.println();
        System.out.print("(1+x)^" + n + " = 1 ");
        for (int i = 1; i <= n; i++) {
            System.out.print(" + " + c(n, i) + "x^" + i);
        }
        System.out.println();
    }

    public static void develmieux(int n) {
        System.out.println();
        System.out.print("(1+x)^" + n + " = 1 ");
        int cni = 1;
        for (int i = 1; i <= n; i++) {
            cni = (cni * (n - i + 1)) / i;
            System.out.print(" + " + cni + "x^" + i);
        }
        System.out.println();
    }

    public static void main(String[] args) {
        Scanner clavier = new Scanner(System.in);
        int n;
        System.out.println("Entrez un entier positif ou nul : ");
        n = clavier.nextInt();
        while (n != 0) {
            System.out.println(n + "! = " + fact(n));
            System.out.println("Entrez un entier positif ou nul : ");
            n = clavier.nextInt();
        }
        if (n == 0) {
            System.out.println(n + "! = " + fact(n));
        }

        for (int i = 0; i <= 16; i++) {
            affichagePuissance(2, i);
        }

        affichagecnp(4, 2);
        affichagecnp(5, 3);
        affichagecnp(32, 28);

        for (int i = 1; i <= 20; i++) {
            affichageexp(1, i);
        }

        System.out.println("Affichage du developpement de (1+x)^n");
        System.out.print("n ? ");
        n = clavier.nextInt();
        while (n > 0) {
            devel(n);
            develmieux(n);
            System.out.print("n ? ");
            n = clavier.nextInt();
        }

    }
}
