package scrabble;

public class Jeton {
    private char lettre;
    static private int nbJetons = 0;

    public Jeton(char lettre) {
        this.lettre = lettre;
        nbJetons++;
    }

    public Jeton() {
        this('*');
    }

    public char getLettre() {
        return lettre;
    }

    static public int getNbJetons() {
        return nbJetons;
    }

    public void affiche() {
        System.out.print("["+lettre+"]");
    }
}
