package exo3;

/**
 * @author yvan.maillot
 */
public class ComplementBidon {
    private final static int[] tableDes737;

    static {
        tableDes737 = new int[100];

        for(int i = 0; i < tableDes737.length; i++) {
            tableDes737[i] = i*737;
        }
    }

    public static int get(int i) {
        return tableDes737[i];
    }
}
