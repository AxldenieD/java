package exo4;

/**
 * Un point est représenté par ses coordonnées cartésiennes
 * et ses coordonnées polaires.
 * @author Maillot
 */
public class Point {

    private double x, y;
    private double module, theta;
    private static int nbInstances;
    
    static {
        nbInstances = 0;
    }

    {
        nbInstances++;
    }

    /**
     * Construire un point à l'aide de ses coordonnées cartésiennes
     * @param x l'absisse 
     * @param y l'ordonnée
     */
    public Point(double x, double y) {
        //nbInstances++;
        this.x = x;
        this.y = y;
        cartesienPolaire();
    }

    /**
     * Point par défaut de coordonnées (0,0)
     */
    public Point() {
        this(0.0, 0.0);
    }

    public static int getNbInstances() {
        return nbInstances;
    }

    private void cartesienPolaire() {
        module = Math.sqrt(x * x + y * y);
        theta = Math.atan2(y, x);
    }

    private void polaireCartesien() {
        x = module * Math.cos(theta);
        y = module * Math.sin(theta);
    }

    /**
     * Construire un point soit à l'aide de ses coordonnées cartésiennes soit à
     * l'aide de ses coordonnées polaires
     * @param roux module ou absisse (selon le choix, polaire ou non)
     * @param thetaouy angle ou ordonnée (selon le choix, polaire ou non)
     * @param polaire, true pour polaire, faux pour cartésien
     */
    public Point(double roux, double thetaouy, boolean polaire) {
        //nbInstances++;
        if (polaire) {
            module = roux;
            theta = thetaouy;
            polaireCartesien();
        } else {
            x = roux;
            y = thetaouy;
            cartesienPolaire();
        }
    }

    public double getModule() {
        return module;
    }

    public void setModule(double module) {
        this.module = module;
        polaireCartesien();
    }

    public double getTheta() {
        return theta;
    }

    public void setTheta(double theta) {
        this.theta = theta;
        polaireCartesien();
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
        cartesienPolaire();
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
        cartesienPolaire();
    }

    /**
     * Appliquer une rotation par rapport à l'origine
     * @param angle angle de rotation
     */
    public void rotation(double angle) {
        theta += angle;
        polaireCartesien();
    }

    /**
     * Appliquer une rotation par rapport à l'origine
     * @param angle angle de rotation
     */
    public void translation(double dx, double dy) {
        x += dx;
        y += dy;
        cartesienPolaire();
    }
}
