package exo4;

/**
 * @author yvan.maillot
 */
public class Main {
    public static void main(String[] args) {
        Point a = new Point(2.0, 1.5);
        Point b = new Point(4.5, 6.2);
        Point c = new Point(5.2, 1.0);
        Polygone p;
        p = new Polygone(a, b, c);
        Polygone q = new Polygone(a, b, new Point(4,6), new Point(5,7));
        System.out.println(q.getPoint(5));
    }
}
