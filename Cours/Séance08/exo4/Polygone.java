package exo4;

/**
 * @author yvan.maillot
 */
public class Polygone {
    private Point[] point;
    public final int nbSommets;

    public Polygone(Point a, Point b, Point c, Point... lp) {
        point = new Point[3+lp.length];
        point[0] = a;
        point[1] = b;
        point[2] = c;

        for (int i = 0; i < lp.length; i++) {
            point[i+3] = lp[i];
        }
        nbSommets = point.length;

    }

    public Point getPoint(int i) {
        return point[i];
    }

    public void setPoint(int i, Point point) {
        this.point[i] = point;
    }

    public void translation(double dx, double dy) {
        for(int i = 0; i < point.length; i++) {
            point[i].translation(dx, dy);
        }
    }

    public void rotation(double angle) {
        for(Point p : point) {
            p.rotation(angle);
        }
    }
}
