package exo2;

/**
 * @author yvan.maillot
 */
public class PourVoirLeRM {

    private static int nbInstances = 0;
    private int numero;

    {
        nbInstances++;
        numero = nbInstances;
    }

    public PourVoirLeRM() {
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        System.out.println("L'instance détruite est la " + numero);
    }
}
