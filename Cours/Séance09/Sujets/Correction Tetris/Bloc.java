package tetris;

import java.util.Random;

public class Bloc {
    private int nbCases;
    private static Random rand = new Random();
    
    public Bloc() {
        nbCases = rand.nextInt(4) + 1;
    }
    
    public int getNbCases() {
        return nbCases;
    }
}
