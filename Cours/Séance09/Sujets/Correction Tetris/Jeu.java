package tetris;

import java.util.Scanner;

public class Jeu {
    private Colonne[] colonnes;
    private int points;
    
    public Jeu(int c1, int c2, int... cn) {
        colonnes = new Colonne[2 + cn.length];
        colonnes[0] = new Colonne(c1);
        colonnes[1] = new Colonne(c2);
        int i = 2;
        for(int c : cn) {
            colonnes[i++] = new Colonne(c);
        }
        points = 0;
    }
    
    public int getPoints() {
        return points;
    }
    
    public void joueUnCoup() {
        // Génération d'un nouveau bloc
        Bloc b = new Bloc();
        // Présentation du bloc et des colonnes au joueur
        System.out.println("Nouveau bloc de " + b.getNbCases() + " case(s)");
        System.out.println("Dans quelle colonne le placer ?");
        int i = 1;
        for (Colonne c : colonnes) {
            System.out.println("Colonne " + i + " : " + c);
            i++;
        }
        // Récupération du choix du joueur
        Scanner lire = new Scanner(System.in);
        i = lire.nextInt();
        while ((i < 1) || (i > colonnes.length) || !colonnes[i - 1].estRemplissable()) {
            System.out.println("Colonne inexistante ou non remplissable, reessayez !");
            i = lire.nextInt();
        }
        // Ajout du bloc à la colonne choisie
        if (colonnes[i - 1].ajoute(b)) {
            // Ajout d'un point si la colonne se vide
            points++;
        }
        // Affichage du nombre de points
        System.out.println("Vous avez " + points + " points");
        System.out.println();
    }
    
    public void joueUnePartie() {
        for (int i = 0; i < colonnes.length; i++) {
            while (colonnes[i].estRemplissable()) {
                joueUnCoup();
            }
        }
        System.out.println("Le jeu est terminé !");
    }
    
    public static void main(String[] args) {
        Jeu jeu = new Jeu(7, 8, 9);
        jeu.joueUnePartie();
    }
}
