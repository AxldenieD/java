/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package utilitaire;

import sudoku.Case;

/**
 *
 * @author maillot
 */
public class Testeur {
    private static boolean[] test = new boolean[9];
    
    private void initialise() {
        for(int i = 0; i < test.length; i++) {
            test[i] = false;
        }
    }
    
    private boolean ligneValide(Case[][] grille, int l) {
        initialise();
        for(Case c : grille[l]) {
            if (!c.estRemplie())
                return false;
            if (test['1'-c.getChiffre()])
                return false;
            test['1'-c.getChiffre()] = true;
        }
        return true;
    }
    
    private boolean colonneValide(Case[][] grille, int c) {
        initialise();
        for(int l = 0; l < 9; l++) {
            if (!grille[l][c].estRemplie())
                return false;
            if (test['1'-grille[l][c].getChiffre()])
                return false;
            test['1'-grille[l][c].getChiffre()] = true;
        }
        return true;
    }
    
}
