package tris;

/**
 * @author yvan.maillot
 */
public class Tris {

    public static void triInsertion(int[] t) {
        for (int i = 1; i < t.length; i++) //Insérer l’élément d’indice i dans t[0..i]
        {
            inserer(t, i);
        }
    }

    public static void inserer(int[] t, int p) {
        int e = t[p];
        int i = p - 1;
        while (i >= 0 && t[i] > e) {
            t[i + 1] = t[i];
            i--;
        }
        t[i + 1] = e;
    }

    public static void triSelection(int[] t) {
        for (int i = 0; i < t.length - 1; i++) // Calculer dans j l’indice du plus petit
        // élément dans t[i.. t.length-1]
        // Echanger t[i] avec t[j]
        {
            echange(i, imin(t, i), t);
        }
    }

    public static void echange(int i1, int i2, int[] t) {
        int a = t[i1];
        t[i1] = t[i2];
        t[i2] = a;
    }

    public static int imin(int[] t, int p) {
        int im = p;
        for (int i = p + 1; i < t.length; i++) {
            if (t[i] < t[im]) {
                im = i;
            }
        }
        return im;
    }
}
