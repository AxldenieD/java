package tris;

import java.util.Random;

/**
 * @author yvan.maillot
 */
public class TestTris {
    public static void main(String[] args) {
        int[] t = new int[100];
        Random r = new Random();
        
        for(int i = 0; i < t.length; i++) {
            t[i] = r.nextInt(60)+1;
        }
        
        for(int e : t) {
            System.out.print(e+", ");
        }
        System.out.println();
        Tris.triSelection(t);
        
        for(int e : t) {
            System.out.print(e+", ");
        }
        System.out.println();
        
        for(int i = 0; i < t.length; i++) {
            t[i] = r.nextInt(60)+1;
        }

        for(int e : t) {
            System.out.print(e+", ");
        }
        System.out.println();
        Tris.triInsertion(t);

        for(int e : t) {
            System.out.print(e+", ");
        }
        System.out.println();

        
    }
}
