package scrabble;

/**
 * @author yvan
 */
public class Reglet {

    private Jeton[] reglet;
    private int n;

    public Reglet() {
        reglet = new Jeton[7];
        n = 0;
    }

    public boolean ajoute(char c) {
        if (n < 7 && c >= 'A' && c <= 'Z') {
            reglet[n] = new Jeton(c);
            n++;
            return true;
        } else {
            return false;
        }
    }

    private int position(char c) {
        for (int i = 0; i < n; i++) {
            if (reglet[i].getLettre() == c) {
                return i;
            }
        }
        return -1;
    }

    public boolean retire(char c) {
        // On récupère la position de c dans le tableau reglet
        int p = position(c);
        if (p == -1) {
            // Si la lettre n'est pas trouvée, on retourne false
            return false;
        } else {
            // Sinon:
            // On décrémente le nombre de jetons présents dans la reglet
            n--;
            // On écrase la lettre d'indice p par la dernière lettre de la reglet
            reglet[p] = reglet[n];
            // On retourne true (on a réussi à retirer la lettre)
            return true;
        }
    }

    // On cherche à savoir si le mot s peut s'écrire avec les lettres présentes
    // dans la reglet
    public boolean secrit(String s) {
        // On créé une reglet vide temporaire
        Reglet r = new Reglet();
        // On ajoute dans notre reglet temporaire toutes les lettres de notre
        // attribut reglet
        for (int i = 0; i < n; i++) {
            r.ajoute(reglet[i].getLettre());
        }

        // Pour toutes les lettre du mot s...
        for (int i = 0; i < s.length(); i++) {
            // on teste si cette lettre est présente dans la reglet temporaire
            // (si l'on arrive à retirer cette lettre de la reglet temporaire)
            if (!r.retire(s.charAt(i))) {
                // Si la lettre n'y est pas, on retourne false
                // (il nous manque cette lettre pour former le mot s)
                return false;
            }
        }
        // Au sortir de la boucle, on sait que toutes les lettres du mots s
        // étaient présentent dans la reglet temporaire.
        // Le mot s peut donc être formé à partir des lettres de la reglet,
        // et on retourne true
        return true;
    }

    public static void main(String[] args) {
        Reglet r = new Reglet();
        r.ajoute('R');
        r.ajoute('E');
        r.ajoute('S');
        r.ajoute('E');
        r.ajoute('A');
        r.ajoute('U');
        r.ajoute('X');

        System.out.println(r.secrit("RESEAUX"));
        System.out.println(r.secrit("ROSEAUX"));
        System.out.println(r.secrit("AXERAS"));
        System.out.println(r.secrit("AXER"));
    }
}
