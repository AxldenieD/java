package point;

/**
 * @author Maillot
 */
public class Triangle {
    private Point a, b, c;

    public Triangle(Point a, Point b, Point c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public Triangle(double xa, double ya,double xb, double yb,double xc, double yc) {
        this(new Point(xa, ya), new Point(xb, yb),new Point(xc, yc));
    }

    public void rotation(double angle) {
        a.rotation(angle);
        b.rotation(angle);
        c.rotation(angle);
    }

    public void translation(double dx, double dy) {
        a.translation(dx, dy);
        b.translation(dx, dy);
        c.translation(dx, dy);
    }
}
